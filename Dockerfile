FROM python:3.6
MAINTAINER Vio
ADD . vk_group_loader_container
WORKDIR vk_group_loader_container
COPY . /vk_group_loader_container/
RUN pip install -r requirements.txt
EXPOSE 9999
CMD ["python3.6", "main.py" ]