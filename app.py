from dataloader import *
import notifier as nf

try:
    fields = ['id, first_name, last_name, sex, verified, bdate']
    get_members(97751087, 'rambler', fields)
    get_members(36023798, 'ramblermail', fields)
    get_members(86125491, 'horoscopesrambler', fields)
    get_members(1331201, 'championat', fields)
    get_members(147610669, 'championat_auto', fields)
    get_members(77903189, 'championat_cybersport', fields)
    get_members(25072924, 'livejournal', fields)
    get_members(1672730, 'afisha', fields)

except Exception as e:
    text_to_send = "Error! VK group dataloader is broken" 
    nf.send(msg=text_to_send, chat_id='me')

# vk.com/rambler 97751087
# vk.com/ramblermail 36023798
# vk.com/horoscopesrambler 86125491
# vk.com/championat 1331201
# vk.com/championat.auto 147610669
# vk.com/championat_cybersport 77903189
# vk.com/livejournal 25072924
# vk.com/afisha 1672730
