import pandas as pd
import sqlite3
import vk
import datetime

token = "f26de404f26de404f26de4049bf21a122fff26df26de40492c7a30c42a153329f7bee06"
session = vk.Session(access_token=token)
vk_api = vk.API(session, v=5.92)

def get_member_info(id, fields):
    response = vk_api.users.get(user_ids = id, fields = fields)
    return response

def get_members(groupid, group_name, fields):
    first = vk_api.groups.getMembers(group_id=groupid, v=5.92) 
    data = first["items"]
    count = first["count"] // 1000 

    for i in range(1, count+1):  
        data = vk_api.groups.getMembers(group_id=groupid, v=5.92, offset=i*1000)["items"]
        a = get_member_info(data, fields)
        df = pd.DataFrame.from_dict(a)
        df['insert_dt'] = datetime.date.today().strftime("%Y-%m-%d")
        data_insert(df, group_name)

def data_insert(df, table_name):
    conn = sqlite3.connect('database/user_db.db')
    c = conn.cursor()
    df.to_sql(name=table_name, con=conn, if_exists='append', index=False)
    c.execute(f"select * from {table_name}")
    res = c.fetchall()