imageName=vk_group_loader_image
containerName=vk_group_loader_container

echo Delete old container...
docker rm -f $containerName

echo Delete old image
docker rmi $(docker images -q $imageName | uniq)

echo Build new Image
docker build -t $imageName  .

echo Run new container...
docker run --restart unless-stopped --network="host" -p 9999:9999 -itd --name $containerName $imageName